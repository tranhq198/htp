
if($(window).width() < 767){
  $('.add__carousel').addClass('owl-carousel owl-theme')
  $('.add__carousel').removeClass('row')
  $('.remove__class').removeClass('col-md-4')
}

$('.owl-carousel').owlCarousel({
  loop:false,
  margin:1,
  nav:false,
  dots: false,
  responsive:{
    0:{
        items:1
    },
    600:{
        items:2
    },
    1000:{
        items:3
    }
  }
})
