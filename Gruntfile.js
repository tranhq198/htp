module.exports = function(grunt) {
  var sourceCssPath = "src/scss",
    sourcrJsPath = "src/js",
    outputCssPath = "public/css",
    outputJsPath = "public/js",
    htmlFolder = "public";
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    connect: {
      server: {
        options: {
          livereload: true,
          base: 'public',
          port: 8080
        }
      }
    },
    sass: {
      dev: {
        files: [
          {
            expand: true,
            cwd: sourceCssPath,
            src: ["style.scss", "!_*.scss"],
            dest: outputCssPath,
            ext: ".css"
          }
        ]
      },
      prod: {
        options: {
          style: "compressed",
          noSourceMap: true
        },
        files: [
          {
            expand: true,
            cwd: sourceCssPath,
            src: ["style.scss"],
            dest: outputCssPath,
            ext: ".css"
          }
        ]
      }
    },
    html_include: {
      default_options: {
        options: {
          workingDir: 'src',
        },
        files: {
          'public': 'src/*.html',
        },
      }
    },
    copy: {
      images: {
        expand: true,
        flatten: true,
        dest: './public/images',
        src: './src/images/*',
        filter: 'isFile'
      },
      fonts: {
        expand: true,
        flatten: true,
        dest: './public/fonts',
        src: './src/fonts/*',
        filter: 'isFile'
      }
    },
    clean: {
      files: {
        src: ['public/*']
      }
    },
    watch: {
      preset: {
        files: [sourceCssPath + "/**/*.scss"],
        tasks: ["sass:dev"],
        options: {
          spawn: false,
          livereload: true
        }
      },
      browserify: {
        files: [sourcrJsPath + "/**/*.js"],
        tasks: ["browserify:dev"],
        options: {
          spawn: false,
          livereload: true
        }
      },
      html_include: {
        files: ["src/**/*.html", "src/*.html"],
        tasks: ["html_include"]
      },
      image: {
        files: ["src/images/*", "src/fonts/*"],
        tasks: ["copy"]
      }
    },
    browserify: {
      dev: {
        files: [
          {
            expand: true,
            cwd: sourcrJsPath,
            src: ["*.js"],
            dest: outputJsPath,
            ext: ".js"
          }
        ],
        options: {
          browserifyOptions: { debug: true },
          transform: [["babelify", { presets: ["es2015"] }]]
        }
      },
      prod: {
        src: [sourcrJsPath + "/*.js"],
        dest: outputJsPath + "/*.min.js",
        options: {
          browserifyOptions: { debug: false },
          transform: [["babelify", { presets: ["es2015"] }]],
          plugin: [["minifyify", { map: false }]]
        }
      }
    }
  });
  grunt.loadTasks('tasks');

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks("grunt-browserify");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask("dev", ["sass:dev", "browserify:dev", "html_include", "copy", "connect:server", "watch"]);
  grunt.registerTask("prod", ["sass:prod", "browserify:prod"]);
};
